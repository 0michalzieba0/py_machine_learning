import pandas

csv_data = pandas.read_csv(
    'https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data')

print("Length: " + len(csv_data))
